                            D               2020.1.4f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                       \       ŕyŻ     `       ä                                                                                                                                            ŕyŻ                                                                                    EventManager}  using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Event Manager
/// Singleton managing all events in scene
/// </summary>
public class EventManager : MonoBehaviour
{
    /*>> Unless you need to define new types of events, it is best to leave this as be. <<*/

    private Dictionary<string, UnityAction> events;
    private Dictionary<string, UnityAction<int>> intEvents;


    private static EventManager eventManager;

    /// <summary>
    /// Makes certain only one event manager exists
    /// </summary>
    public static EventManager instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
                if (eventManager)
                {
                    eventManager.initalize();
                }
            }
            return eventManager;
        }
    }

    /// <summary>
    /// Sets up the dictionary of events.
    /// </summary>
    private void initalize()
    {
        if (events == null)
        {
            events = new Dictionary<string, UnityAction>();
        }
        if (intEvents == null)
        {
            intEvents = new Dictionary<string, UnityAction<int>>();
        }
    }

    /// <summary>
    /// Instructs the event manager to listen for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StartListening(string name, UnityAction action)
    {
        if (instance != null && instance.events.ContainsKey(name))
        {
            instance.events[name] += action;
        }
        else
        {
            instance.events.Add(name, action);
        }
    }

    /// <summary>
    /// Instructs the event manager to listen for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StartListening(string name, UnityAction<int> action)
    {
        if (instance != null && instance.intEvents.ContainsKey(name))
        {
            instance.intEvents[name] += action;
        }
        else
        {
            instance.intEvents.Add(name, action);
        }
    }

    /// <summary>
    /// Instructs event manager to stop listening for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StopListening(string name, UnityAction action)
    {
        if (instance != null && instance.events.ContainsKey(name))
        {
            instance.events[name] -= action;
        }
    }


    /// <summary>
    /// Instructs event manager to stop listening for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StopListening(string name, UnityAction<int> action)
    {
        if (instance != null && instance.intEvents.ContainsKey(name))
        {
            instance.intEvents[name] -= action;
        }
    }


    /// <summary>
    /// Triggers an event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public static void TriggerEvent(string name)
    {
        UnityAction Event = null;
        if (instance != null && instance.events.TryGetValue(name, out Event))
        {
            Event.Invoke();
        }
    }

    /// <summary>
    /// Triggers an event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public static void TriggerEvent(string name, int value)
    {
        UnityAction<int> Event = null;
        if (instance != null && instance.intEvents.TryGetValue(name, out Event))
        {
            Event.Invoke(value);
        }
    }
}
                          EventManager    