﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private GameObject winStreakObj;
    [SerializeField] private Text streakText;

    private int streak;

    // Start is called before the first frame update
    void Start()
    {
        streak = SaveLoad.instance.GetStreak();
        if (streak != 0)
        {
            winStreakObj.SetActive(true);
            streakText.text = streak + "";
        }
    }

}
