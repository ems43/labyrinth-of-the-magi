﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    private static MenuScript eventManager;

    /// <summary>
    /// Makes certain only one event manager exists
    /// </summary>
    public static MenuScript instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(MenuScript)) as MenuScript;
            }
            return eventManager;
        }
    }
    #pragma warning disable 0649
    [SerializeField] private bool inBattle;
    private PlayerMovement player;

    private void Start()
    {
        SaveLoad.instance.setBattle(inBattle);
    }

    public void PauseUnpause()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        } else
        {
            Time.timeScale = 0;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void NextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void OpenClose(GameObject obj)
    {
        obj.SetActive(!obj.activeInHierarchy);
    }

    public void Save(bool didWin)
    {
        SaveLoad.instance.SaveData(didWin);
    }

    public void Cheat(Item item)
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        }
        player.changeStats(item.newHealth, item.newSpd, item.newDamage, item.shotsNum, item.fireSpd, item.shotSpread, item.shotSpd, item.shotRange);
    }
}
