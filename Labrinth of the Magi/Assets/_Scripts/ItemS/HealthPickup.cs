﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    private int percentIncrease = 10;

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag.Equals("Player"))
        {
            EventManager.TriggerEvent("healthBack", percentIncrease);
            Destroy(gameObject);
        }
    }
}
