﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Items", order = 1)]
public class Item : ScriptableObject
{
    public string Name;
    public string Description;

    public int newHealth;
    public float newSpd;
    public int newDamage;
    public int shotsNum;
    public float fireSpd;
    public float shotSpread;
    public float shotSpd;
    public float shotRange;

}
