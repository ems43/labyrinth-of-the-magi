﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSetter : MonoBehaviour
{
    #pragma warning disable 0649
    private static PlayerMovement player;
    private Item item;
    [SerializeField]  private Item[] itemList;
    // Start is called before the first frame update
    void Start()
    {
        int rand = Random.Range(0, itemList.Length - 1);
        item = itemList[rand];
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            print(item.Name + ": " + item.Description);
            player.changeStats(item.newHealth, item.newSpd, item.newDamage, item.shotsNum, item.fireSpd, item.shotSpread, item.shotSpd, item.shotRange);
            Destroy(gameObject);
        }
    }

}
