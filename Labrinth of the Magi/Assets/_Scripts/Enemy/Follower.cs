﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : Enemy
{
    #pragma warning disable 0649
    // Temp values to modify
    [SerializeField] private int tHealth;
    [SerializeField] private int tDamage;
    [SerializeField] private float tSpeed;
    [SerializeField] private GameObject[] tDrops;

    // Start is called before the first frame update
    void Start()
    {
        health = tHealth;
        damage = tDamage;
        speed = tSpeed;
        drops = tDrops;
        me.speed = base.speed;
        me.SetDestination(player.transform.position);
        StartCoroutine("findPlayer");
    }

    IEnumerator findPlayer()
    {
        while (gameObject.activeInHierarchy && player.activeInHierarchy)
        {
            yield return new WaitForSeconds(0.1f);
            if (me.speed > 0 && me.autoBraking)
            {
                me.SetDestination(player.transform.position);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        HitPlayer(other);
    }

    public int getHealth()
    {
        return tHealth;
    }
    
    public float getSpeed()
    {
        return tSpeed;
    }
}
