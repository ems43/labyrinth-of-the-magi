﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private GameObject[] enemies;
    private int rand = 0;

    // Start is called before the first frame update
    void Start()
    {
        rand = Random.Range(0, enemies.Length - 1);
    }

    public void Spawn()
    {
        GameObject obj = Instantiate(enemies[rand], transform.position, Quaternion.identity, gameObject.GetComponentInParent<Transform>());
        Destroy(gameObject);
    }
}
