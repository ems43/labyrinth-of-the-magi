﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BossAI : Enemy
{
    private float Timer;
    #pragma warning disable 0649
    [SerializeField] private bool hasSpawns;
    private float spawnerTimer = 0;
    [SerializeField] private float spawnFrequency;
    [SerializeField] private GameObject spawn;
    [SerializeField] private int howManySpawns;
    private List<GameObject> spawns = new List<GameObject>();

    [SerializeField] private bool doesCharge;
    private float chargeTimer = 0;
    [SerializeField] private float chargeFrequency;

    private Slider healthUI;
    private UnityAction<int> hit;

    private CameraControlls camControls;
    private Follower otherClass;
    private GameObject newMapTrigger;

    private void OnEnable()
    {
        hit = new UnityAction<int>(UpdateUI);
        EventManager.StartListening("BossHit", hit);
    }

    void onDisable()
    {
        EventManager.StopListening("BossHit", hit);
    }

    // Start is called before the first frame update
    void Start()
    {
        camControls = Camera.allCameras[0].GetComponent<CameraControlls>();
        healthUI = camControls.getBossUI().GetComponent<Slider>();
        otherClass = gameObject.GetComponent<Follower>();
        healthUI.gameObject.SetActive(true);
        health = otherClass.getHealth();
        healthUI.maxValue = health;
        healthUI.value = health;
        speed = otherClass.getSpeed();
        StartCoroutine("bossUpdate");
        newMapTrigger = GameObject.FindGameObjectWithTag("MapTrigger");
        newMapTrigger.SetActive(false);
    }

    void UpdateUI(int health)
    {
        if (health <= 0)
        {
            newMapTrigger.SetActive(true);
            healthUI.value = 0;
            healthUI.gameObject.SetActive(false);
            foreach (GameObject g in spawns)
            {
                Destroy(g);
            }
        }
        else
        {
            healthUI.value = health;
        }
    }

    IEnumerator bossUpdate()
    {
        while (gameObject.activeInHierarchy & player.activeInHierarchy)
        {
            Timer = Time.time;
            yield return new WaitForSeconds(0.1f);
            Timer = Time.time - Timer;
            if (hasSpawns)
            {
                spawnerTimer += Timer;
                if (spawnerTimer >= spawnFrequency)
                {
                    spawnerTimer = 0;
                    Spawn();
                }
            }
            if (doesCharge)
            {
                chargeTimer += Timer;
                if (chargeTimer >= chargeFrequency)
                {
                    chargeTimer = 0;
                    doesCharge = false;
                    StartCoroutine("Charge");
                }
            }
            Timer = 0;
        }
    }

    private void Spawn()
    {
        for (int i = 0; i < howManySpawns; i++)
        {
            GameObject sp = Instantiate(spawn, transform.position, Quaternion.identity);
            spawns.Add(sp);
            sp.GetComponent<Follower>().setAsBossSpawn();
        }
    }

    IEnumerator Charge()
    {
        me.speed = 0;
        me.autoBraking = false;
        Vector3 angle = new Vector3(player.transform.position.x - transform.position.x, 0, player.transform.position.z - transform.position.z);
        yield return new WaitForSeconds(1f);
        me.velocity = angle * 20f;
        yield return new WaitForSeconds(0.3f);
        me.velocity = Vector3.zero;
        me.speed = base.speed;
        me.autoBraking = true;
        doesCharge = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        HitPlayer(other);
    }
}
