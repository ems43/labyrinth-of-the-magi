﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    protected static GameObject player;
    protected NavMeshAgent me;
    protected int health;
    protected int damage;
    protected float speed;
    private bool dead = false;

    protected GameObject[] drops;

    private bool isBoss = false;
    private bool bossSpawns = false;

    // Start is called before the first frame update
    void Awake()
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        if (GetComponent<BossAI>())
        {
            isBoss = true;
        }
        me = gameObject.GetComponent<NavMeshAgent>();
    }

    public void setAsBossSpawn()
    {
        bossSpawns = true;
    }

    public void Damaged(int i)
    {
        if (!dead)
        {
            health -= i;
            if (isBoss)
            {
                EventManager.TriggerEvent("BossHit", health);
            }
            if (health <= 0)
            {
                dead = true;
                if (!bossSpawns)
                {
                    DropItems();
                    EventManager.TriggerEvent("EnemyDead");
                }
                Destroy(gameObject);
            }
        }
    }

    protected void HitPlayer(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            EventManager.TriggerEvent("playerHit", damage);
        }
    }

    public void DropItems()
    {
        int dropped = 0;
        float speed = 5;
        float randomChance = 0.15f;
        foreach (GameObject t in drops)
        {
            float rand = Random.Range(0.0f, 1.0f);
            if (rand < randomChance)
            {
                GameObject newPickUp = Instantiate(t, gameObject.transform.position, Quaternion.identity, gameObject.GetComponentInParent<Transform>());
                newPickUp.transform.rotation = Quaternion.Euler(0, transform.position.z + (360 * dropped / (drops.Length)), 0);
                newPickUp.GetComponent<Rigidbody>().velocity = newPickUp.transform.forward * speed;
                newPickUp.transform.rotation = Quaternion.Euler(45, 0, transform.rotation.eulerAngles.z);
                dropped++;
            }
        }
    }
}
