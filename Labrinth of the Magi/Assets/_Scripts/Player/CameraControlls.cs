﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraControlls : MonoBehaviour
{
    #pragma warning disable 0649
    private static GameObject player;
    private List<GameObject> map;

    private GameObject currentRoom;
    private Bounds currentRoomBounds;
    private Bounds roomBounds;
    private List<GameObject> roomSeal;
    private int EnemiesLeft = 0;
    private UnityAction unseal;

    [SerializeField] private MapGeneration mapGen;
    private Vector3 offset = new Vector3(0, 10.5f, -8f);

    private GameObject bossUI;
    [SerializeField] private GameObject deathScreen;

    private UnityAction dead;
    private UnityAction newMap;

    private void OnEnable()
    {
        EventManager.StartListening("EnemyDead", unseal);
        EventManager.StartListening("PlayerDead", dead);
        EventManager.StartListening("NewMap", newMap);
    }

    private void OnDisable()
    {
        EventManager.StopListening("EnemyDead", unseal);
        EventManager.StopListening("PlayerDead", dead);
        EventManager.StopListening("NewMap", newMap);
    }

    private void Awake()
    {
        unseal = new UnityAction(unSealRoom);
        dead = new UnityAction(Dead);
        newMap = new UnityAction(GetNewMap);
    }

    // Start is called before the first frame update
    void Start()
    {
        bossUI = GameObject.FindGameObjectWithTag("BossUI");
        bossUI.SetActive(false);
        player = GameObject.FindGameObjectWithTag("Player");
        GetNewMap();
    }

    void GetNewMap()
    {
        currentRoom = mapGen.getStartRoom();
        map = mapGen.getMap();
        foreach (GameObject g in map)
        {
            if (g != currentRoom)
            {
                g.SetActive(false);
            }
        }
        currentRoom.SetActive(true);
        currentRoomBounds = currentRoom.GetComponentInChildren<BoxCollider>().bounds;
    }

    /// <summary>
    /// Looks for player and follows within room bounds.
    /// </summary>
    void FixedUpdate()
    {
        if (player.activeInHierarchy)
        {
            Vector3 newPos = player.transform.position + offset;
            if (Mathf.Abs(player.transform.position.x - currentRoom.transform.position.x) >= (currentRoomBounds.extents.x / 3f))
            {
                newPos = new Vector3(currentRoom.transform.position.x + (Mathf.Sign(player.transform.position.x - currentRoom.transform.position.x) * (currentRoomBounds.extents.x / 3f)), newPos.y, newPos.z);
            }
            if (Mathf.Abs(player.transform.position.z - currentRoom.transform.position.z) >= (currentRoomBounds.extents.z / 2f))
            {
                newPos = new Vector3(newPos.x, newPos.y, currentRoom.transform.position.z + (Mathf.Sign(player.transform.position.z - currentRoom.transform.position.z) * (currentRoomBounds.extents.z / 2f)) + offset.z);
            }
            transform.position = newPos;
        }
    }


    /// <summary>
    /// Changes the room bounds to the one that the player has walk into.
    /// </summary>
    /// <param name="room"></param>
    /// <param name="seals"></param>
    /// <param name="EnemyNum"></param>
    public void changeRoom(GameObject room, List<GameObject> seals, int EnemyNum)
    {

        currentRoom.SetActive(false);
        currentRoom = room;
        currentRoom.SetActive(true);
        currentRoomBounds = currentRoom.GetComponentInChildren<BoxCollider>().bounds;
        EnemiesLeft = EnemyNum;
        roomSeal = seals;
        if (EnemyNum == 0)
        {
            unSealRoom();
        }

    }

    /// <summary>
    /// All enemies are dead and the player can progress.
    /// </summary>
    private void unSealRoom()
    {
        EnemiesLeft--;
        if (EnemiesLeft <= 0)
        {
            foreach (GameObject g in roomSeal)
            {
                g.SetActive(false);
            }
        }
    }

    public GameObject getBossUI()
    {
        return bossUI;
    }

    private void Dead()
    {
        deathScreen.SetActive(true);
    }
}
