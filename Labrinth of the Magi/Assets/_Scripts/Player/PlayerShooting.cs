﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerShooting : MonoBehaviour
{
    #pragma warning disable 0649
    private Camera cam;

    //Aiming Variables
    //public GameObject angleFinder;
    private Vector3 angle2D;


    //Firing Variables
    private bool fireTime = true;
    private int numOfShots = 1;
    private int damage = 5;
    private float firingSpeed = 1f;
    private float shotSpeed = 10f;
    private float shotSpread = 0;
    private float shotRange = 1;

    //Firing GameObject variables
    [SerializeField] private GameObject projectile;

    private bool isDead = false;

    private UnityAction dead;


    private void OnEnable()
    {
        EventManager.StartListening("PlayerDead", dead);
    }

    private void OnDisable()
    {
        EventManager.StopListening("PlayerDead", dead);
    }

    private void Awake()
    {
        dead = new UnityAction(Dead);
    }

    /// <summary>
    /// Just need to make sure everything interacts correctly and get the camera
    /// Sets the intial weapons ammo capacity to full
    /// </summary>
    private void Start()
    {
        cam = Camera.allCameras[0];
    }

    /// <summary>
    /// Once per frame, checks if mouse is held down
    /// </summary>
    void Update()
    {
        if (!isDead)
        {
            if (Input.GetKey(KeyCode.Mouse0) && fireTime)
            {
                StartCoroutine("fireWait");
                Shoot();
            }
        }
    }


    /// <summary>
    /// Method for creation and manipulation of weapon projectiles. fireMode is for testing the perfered projectile method.
    /// </summary>
    void Shoot()
    {
        // Gets the direction fired by using mouse position
        Vector3 angle = Vector3.zero;
        LayerMask mask = LayerMask.GetMask("Floor");
        Ray angleRay = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit angleHit;
        if (Physics.Raycast(angleRay.origin, angleRay.direction, out angleHit, Mathf.Infinity, mask)) {
            {
                angle2D = new Vector3(angleHit.point.x - transform.position.x, 0, angleHit.point.z - transform.position.z);
            }
        } else
        {
            print("Failed to find Floor.");
            return;
        }

        for (int i = 0; i < numOfShots; i++)
        {
            float spread = 0;
            if (numOfShots > 1)
            {
                int shot = i - (int)Mathf.Floor(numOfShots / 2f);
                spread = Random.Range(-shotSpread, shotSpread) + (shot * (90f / numOfShots));
            }
            //if (fireMode == 0) //2D Sprite Bullet
            //{
            GameObject proj = GameObject.Instantiate(projectile, transform.position, Quaternion.identity);
            playerProjectile projScript = proj.GetComponent<playerProjectile>();
            proj.transform.forward = angle2D;
            proj.transform.Rotate(new Vector3(0, spread, 0));
            projScript.setValues(shotSpeed, damage, shotRange);
        }
    }

    private void Dead()
    {
        isDead = true;
    }

    /// <summary>
    /// Time between shooting a shot and the next. Self Explanitory
    /// </summary>
    /// <returns></returns>
    IEnumerator fireWait()
    {
        if (firingSpeed > 0)
        {
            fireTime = false;
            yield return new WaitForSeconds(firingSpeed);
            fireTime = true;
        }
    }

    public void changeStats(int newDamage, int shotsNum, float fireSpd, float newShotSpread, float shotSpd, float shotRng)
    {
        if (damage + newDamage <= 0)
        {
            damage = 1;
        }
        else
        {
            damage += newDamage;
        }
        if (numOfShots == 1 && shotsNum != 0)
        {
            numOfShots = shotsNum;
        } else if (shotsNum != 0)
        {
            numOfShots *= shotsNum;
        }
        if (firingSpeed + fireSpd >= 3)
        {
            firingSpeed = 3;
        }
        else if (firingSpeed + fireSpd <= 0)
        {
            firingSpeed = 0;
        }
        else
        {
            firingSpeed += fireSpd;
        }
        shotSpread += newShotSpread;
        if (shotSpeed + shotSpd <= 1)
        {
            shotSpeed = 1;
        } else
        {
            shotSpeed += shotSpd;
        }
        if (shotRange + shotRng <= 1)
        {
            shotRange = 1;
        }
        else
        {
            shotRange += shotRng;
        }
    }
}
