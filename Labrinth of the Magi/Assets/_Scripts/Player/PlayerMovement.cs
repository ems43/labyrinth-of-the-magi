﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    private int health = 5;
    private int MaxHealth = 5;
    private bool Invulnerable = false;

    #pragma warning disable 0649
    [SerializeField] private float speed;
    private Vector2 input;
    private Rigidbody rb;
    private SpriteRenderer rend;
    private PlayerShooting shoot;

    private UnityAction<int> damaged;
    private UnityAction<int> healthBack;

    [SerializeField] private Slider healthUI;
    [SerializeField] private GameObject pausedMenu;

    private bool isDead = false;

    private void OnEnable()
    {
        EventManager.StartListening("playerHit", damaged);
        EventManager.StartListening("healthBack", healthBack);
    }

    private void OnDisable()
    {
        EventManager.StopListening("playerHit", damaged);
        EventManager.StopListening("healthBack", healthBack);
    }

    private void Awake()
    {
        damaged = new UnityAction<int>(Damaged);
        healthBack = new UnityAction<int>(Heal);
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<SpriteRenderer>();
        shoot = GetComponent<PlayerShooting>();
        health = MaxHealth;
        healthUI.maxValue = MaxHealth;
        healthUI.value = health;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                MenuScript.instance.OpenClose(pausedMenu);
                MenuScript.instance.PauseUnpause();
            }
        }
    }

    private void FixedUpdate()
    {
        if (!isDead)
        {
            rb.velocity = new Vector3(input.x * speed, 0, input.y * speed);
        }
    }

    private void Damaged(int d)
    {
        if (!Invulnerable)
        {
            health -= d;
            if (health <= 0)
            {
                healthUI.value = 0;
                isDead = true;
                Time.timeScale = 1;
                EventManager.TriggerEvent("PlayerDead");
                gameObject.SetActive(false);
            } else
            {
                healthUI.value = health;
                StartCoroutine("invulnerable");
            }
        }
    }

    IEnumerator invulnerable()
    {
        Invulnerable = true;
        StartCoroutine("flicker");
        yield return new WaitForSeconds(3f);
        Invulnerable = false;
    }

    IEnumerator flicker()
    {
        while (Invulnerable)
        {
            rend.enabled = false;
            yield return new WaitForSeconds(0.1f);
            rend.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void Heal(int i)
    {
        float percent = i / 100;
        if (health + (health * percent) > MaxHealth)
        {
            health = MaxHealth;
        } else
        {
            health += Mathf.CeilToInt(health * percent); //Rounds up to nearest int, stops increases of 0;
        }
        healthUI.value = health;
    }

    public void changeStats(int newHealth, float newSpd, int newDamage, int shotsNum, float fireSpd, float shotSpread, float shotSpd, float shotRange)
    {
        MaxHealth += newHealth;
        health = MaxHealth;
        healthUI.maxValue = MaxHealth;
        healthUI.value = health;
        if (speed + newSpd <= 0)
        {
            speed = 1f;
        }
        else
        {
            speed += newSpd;
        }
        shoot.changeStats(newDamage, shotsNum, fireSpd, shotSpread, shotSpd, shotRange);
    }
}
