﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerProjectile : MonoBehaviour
{
    private Rigidbody rb;
    private float speed;
    private int damage;
    private float range;

    public void setValues(float spd, int dmg, float rng)
    {
        speed = spd;
        damage = dmg;
        range = rng;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Move();
        Destroy(gameObject, range);
    }

    void Move()
    {
        rb.velocity = transform.forward * speed;
        transform.rotation = Quaternion.Euler(45, 0, transform.rotation.eulerAngles.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Enemy"))
        {
            other.GetComponentInParent<Enemy>().Damaged(damage);
            Destroy(gameObject);
        }
        else if (!other.tag.Equals("Player"))
        {
            Destroy(gameObject);
        }
    }


}
