﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeRoom : MonoBehaviour
{

    private static CameraControlls controller;
    private GameObject room = null;
    private Spawner[] spawners;
    private List<GameObject> seals = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        if (!controller)
        {
            controller = Camera.allCameras[0].GetComponent<CameraControlls>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            foreach (Spawner s in spawners)
            {
                s.Spawn();
            }
            controller.changeRoom(room, seals, spawners.Length);
            spawners = new Spawner[0];
        }
    }

    /// <summary>
    /// Sets the room to be the one called if the trigger attached to this is called.
    /// </summary>
    /// <param name="g"></param>
    public void setRoom(GameObject g)
    {
        room = g;
        spawners = room.GetComponentsInChildren<Spawner>();
        Transform[] walls = room.GetComponentsInChildren<Transform>();
        List<Transform> objects = new List<Transform>();
        foreach (Transform t in walls)
        {
            if (t.gameObject.tag.Equals("SealWall"))
            {
                seals.Add(t.gameObject);
            }
        }
    }
}
