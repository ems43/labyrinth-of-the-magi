﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMap : MonoBehaviour
{

    /// <summary>
    /// Sole purpose being the trigger that calls the new map method.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            EventManager.TriggerEvent("NewMap");
        }
    }
}
