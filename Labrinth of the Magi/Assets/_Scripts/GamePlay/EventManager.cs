﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Event Manager
/// Singleton managing all events in scene
/// </summary>
public class EventManager : MonoBehaviour
{
    /*>> Unless you need to define new types of events, it is best to leave this as be. <<*/

    private Dictionary<string, UnityAction> events;
    private Dictionary<string, UnityAction<int>> intEvents;


    private static EventManager eventManager;

    /// <summary>
    /// Makes certain only one event manager exists
    /// </summary>
    public static EventManager instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
                if (eventManager)
                {
                    eventManager.initalize();
                }
            }
            return eventManager;
        }
    }

    /// <summary>
    /// Sets up the dictionary of events.
    /// </summary>
    private void initalize()
    {
        if (events == null)
        {
            events = new Dictionary<string, UnityAction>();
        }
        if (intEvents == null)
        {
            intEvents = new Dictionary<string, UnityAction<int>>();
        }
    }

    /// <summary>
    /// Instructs the event manager to listen for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StartListening(string name, UnityAction action)
    {
        if (instance != null && instance.events.ContainsKey(name))
        {
            instance.events[name] += action;
        }
        else
        {
            instance.events.Add(name, action);
        }
    }

    /// <summary>
    /// Instructs the event manager to listen for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StartListening(string name, UnityAction<int> action)
    {
        if (instance != null && instance.intEvents.ContainsKey(name))
        {
            instance.intEvents[name] += action;
        }
        else
        {
            instance.intEvents.Add(name, action);
        }
    }

    /// <summary>
    /// Instructs event manager to stop listening for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StopListening(string name, UnityAction action)
    {
        if (instance != null && instance.events.ContainsKey(name))
        {
            instance.events[name] -= action;
        }
    }


    /// <summary>
    /// Instructs event manager to stop listening for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StopListening(string name, UnityAction<int> action)
    {
        if (instance != null && instance.intEvents.ContainsKey(name))
        {
            instance.intEvents[name] -= action;
        }
    }


    /// <summary>
    /// Triggers an event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public static void TriggerEvent(string name)
    {
        UnityAction Event = null;
        if (instance != null && instance.events.TryGetValue(name, out Event))
        {
            Event.Invoke();
        }
    }

    /// <summary>
    /// Triggers an event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public static void TriggerEvent(string name, int value)
    {
        UnityAction<int> Event = null;
        if (instance != null && instance.intEvents.TryGetValue(name, out Event))
        {
            Event.Invoke(value);
        }
    }
}
