﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class SaveLoad : MonoBehaviour
{

    private static SaveLoad eventManager;

    /// <summary>
    /// Makes certain only one event manager exists
    /// </summary>
    public static SaveLoad instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(SaveLoad)) as SaveLoad;
            }
            return eventManager;
        }
    }

    private int winStreak;
    private int totalWin;
    private int totalLose;
    private bool inBattle;

    public int GetStreak()
    {
        return winStreak;
    }

    public int getWins()
    {
        return totalWin;
    }

    public int getLoses()
    {
        return totalLose;
    }

    public void setBattle(bool isInBattle)
    {
        inBattle = isInBattle;
    }

    private void Awake()
    {
        if (this != instance)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            Load();
        }
    }

    /// <summary>
    /// Saves in case the player tries to quit out of the application
    /// </summary>
    private void OnApplicationQuit()
    {
        if (inBattle)
        {
            SaveData(false);
        }
    }

    /// <summary>
    /// Sets data based on if the player caused actions that would be a win or lose condition.
    /// </summary>
    /// <param name="didWin"></param>
    public void SaveData(bool didWin)
    {
        if (didWin)
        {
            totalWin++;
        } else
        {
            totalLose++;
        }

        if (winStreak >= 0 && didWin)
        {
            winStreak++;
        } else if (winStreak <= 0 && !didWin)
        {
            winStreak--;
        }
        else if (didWin)
        {
            winStreak = 1;
        } else
        {
            winStreak = -1;
        }
        Save();
    }

    /// <summary>
    /// Saves data to database.
    /// </summary>
    private void Save()
    {
        string connect = "URI=file:" + Application.dataPath + "/Save/GameSave.db"; //Path to database.
        IDbConnection dbConnect;
        dbConnect = (IDbConnection)new SqliteConnection(connect);
        dbConnect.Open(); //Open connection to the database.
        IDbCommand dbCommand = dbConnect.CreateCommand();
        string query = "DROP TABLE GameSave";           //Drops data saved
        dbCommand.CommandText = query;
        IDataReader reader = dbCommand.ExecuteReader();
        //Creates new table
        query =
              "CREATE TABLE IF NOT EXISTS GameSave " +
              "(id INTEGER PRIMARY KEY," +
              "winStreak INTEGER," +
              "totalWin INTEGER," +
              "totalLose INTEGER)";

        reader.Close();
        reader = null;

        dbCommand.CommandText = query;
        reader = dbCommand.ExecuteReader();

        //Inserts Data into table
        query = "INSERT INTO GAMESAVE(winStreak, totalWin, totalLose)" +
            "VALUES ("+winStreak+","+totalWin+","+totalLose+")";
        reader.Close();
        reader = null;

        dbCommand.CommandText = query;
        reader = dbCommand.ExecuteReader();

        reader.Close();
        reader = null;
        dbCommand.Dispose();
        dbCommand = null;
        dbConnect.Close();
        dbConnect = null;
    }

    /// <summary>
    /// Loads data from database.
    /// </summary>
    private void Load()
    {
        try
        {
            string connect = "URI=file:" + Application.dataPath + "/Save/GameSave.db"; //Path to database.
            IDbConnection dbConnect;
            dbConnect = (IDbConnection)new SqliteConnection(connect);
            dbConnect.Open(); //Open connection to the database.
            IDbCommand dbCommand = dbConnect.CreateCommand();
            IDataReader reader;
            string query = "SELECT winStreak, totalWin, totalLose FROM GameSave";
            dbCommand.CommandText = query;
            reader = dbCommand.ExecuteReader();
            while (reader.Read())
            {
                int Streak = reader.GetInt32(0);
                int Win = reader.GetInt32(1);
                int Lose = reader.GetInt32(2);

                winStreak = Streak;
                totalWin = Win;
                totalLose = Lose;
            }
            reader.Close();
            reader = null;
            dbCommand.Dispose();
            dbCommand = null;
            dbConnect.Close();
            dbConnect = null;

        } catch (Exception)
        {
            IDbConnection dbConnect;
            IDbCommand dbCommand;
            IDataReader reader;

            string connect = "URI=file:" + Application.dataPath + "/Save/GameSave.db"; //Path to database.
            dbConnect = (IDbConnection)new SqliteConnection(connect);
            dbConnect.Open(); //Open connection to the database.
            dbCommand = dbConnect.CreateCommand();

            string query =
              "CREATE TABLE IF NOT EXISTS GameSave " +
              "(id INTEGER PRIMARY KEY," +
              "winStreak INTEGER," +
              "totalWin INTEGER," +
              "totalLose INTEGER)";

            dbCommand.CommandText = query;
            reader = dbCommand.ExecuteReader();

            reader.Close();
            reader = null;
            dbCommand.Dispose();
            dbCommand = null;
            dbConnect.Close();
            dbConnect = null;
        }
    }
}
