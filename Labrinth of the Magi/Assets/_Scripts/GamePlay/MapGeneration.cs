﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class MapGeneration : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private int mazeSize;
    private int[,] maze;
    private List<GameObject> map = new List<GameObject>();
    private Vector2 start;
    private Vector2 end;
    private Vector2 item;
    [SerializeField] private float roomChance;
    [SerializeField] private GameObject bossRoom;
    [SerializeField] private GameObject startRoom;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject[] room;
    private GameObject parent;
    [SerializeField] private GameObject openWall;
    [SerializeField] private GameObject closedWall;
    [SerializeField] private GameObject trigger;
    private GameObject startingRoom;
    private UnityAction newMap;
    [SerializeField] private GameObject itemRoom;

    private List<NavMeshSurface> surface;

    private void OnEnable()
    {
        EventManager.StartListening("NewMap", newMap);
    }

    private void OnDisable()
    {
        EventManager.StopListening("NewMap", newMap);
    }

    private void Awake()
    {
        newMap = new UnityAction(NewMap);
        NewMap();
    }

    /// <summary>
    /// Starts a new map
    /// </summary>
    void NewMap()
    {
        surface = new List<NavMeshSurface>();
        mazeSize++;
        player.SetActive(false);
        Time.timeScale = 1;
        if (parent)
        {
            Destroy(parent);
            map.Clear();
        }
        int stop = 0;
        bool work = false;
        while (stop < int.MaxValue - 1 && !work)
        {
            StartEndRandomizer();
            maze = new int[mazeSize, mazeSize];
            for (int i = 0; i < mazeSize; i++)
            {
                for (int q = 0; q < mazeSize; q++)
                {
                    float rand = Random.Range(0.0f, 1.0f);
                    if (rand < roomChance)
                    {
                        maze[i, q] = 1;
                    }
                    else
                    {
                        maze[i, q] = 0;
                    }
                }
            }
            maze[(int)start.x, (int)start.y] = 1;
            maze[(int)end.x, (int)end.y] = 1;
            maze[(int)item.x, (int)item.y] = 1;
            if (MazeTester(end))
            {
                if (MazeTester(item))
                {
                    work = true;
                    MazeBuilder();
                }
            }
            else
            {
                stop++;
                if (stop >= int.MaxValue - 1)
                {
                    print("After the maximum number of builds, no build could be found that is able to be completed.");
                    break;
                }
            }
        }
        foreach(NavMeshSurface n in surface)
        {
            n.BuildNavMesh();
        }
    }

    /// <summary>
    /// Randomizes the start, end, and item room.
    /// </summary>
    void StartEndRandomizer()
    {
        start = new Vector2(Random.Range(0, mazeSize), Random.Range(0, mazeSize));
        end = new Vector2(Random.Range(0, mazeSize), Random.Range(0, mazeSize));
        item = new Vector2(Random.Range(0, mazeSize), Random.Range(0, mazeSize));
        if (start == end || start == item || item == end)
        {
            StartEndRandomizer();
        }
    }

    /// <summary>
    /// Builds the maze via prefabs and sets player position.
    /// </summary>
    void MazeBuilder()
    {
        parent = new GameObject();
        for (int i = 0; i < mazeSize; i++)
        {
            for (int q = 0; q < mazeSize; q++)
            {
                
                if (end.x == i && end.y == q)
                {
                    GameObject obj = Instantiate(bossRoom, parent.transform);
                    BoxCollider col = obj.GetComponentInChildren<BoxCollider>();
                    obj.transform.position = new Vector3(-(col.bounds.extents.x + (col.bounds.size.x * i)), 0, col.bounds.extents.z + (col.bounds.size.z * q));
                    map.Add(obj);
                    surface.Add(obj.GetComponent<NavMeshSurface>());
                    GameObject t = Instantiate(trigger, parent.transform);
                    t.transform.localPosition = obj.transform.position;
                    Walls(i, q, obj);
                    t.GetComponent<changeRoom>().setRoom(obj);
                }
                else if (start.x == i && start.y == q)
                {
                    GameObject obj = Instantiate(startRoom, parent.transform);
                    BoxCollider col = obj.GetComponentInChildren<BoxCollider>();
                    obj.transform.position = new Vector3(-(col.bounds.extents.x + (col.bounds.size.x * i)), 0, col.bounds.extents.z + (col.bounds.size.z * q));
                    player.SetActive(true);
                    player.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 0.3f, obj.transform.position.z);
                    startingRoom = obj;
                    map.Add(obj);
                    GameObject t = Instantiate(trigger, parent.transform);
                    t.transform.localPosition = obj.transform.position;
                    Walls(i, q, obj);
                    t.GetComponent<changeRoom>().setRoom(obj);
                } else if (item.x == i && item.y == q)
                {
                    GameObject obj = Instantiate(itemRoom, parent.transform);
                    BoxCollider col = obj.GetComponentInChildren<BoxCollider>();
                    obj.transform.position = new Vector3(-(col.bounds.extents.x + (col.bounds.size.x * i)), 0, col.bounds.extents.z + (col.bounds.size.z * q));
                    map.Add(obj);
                    surface.Add(obj.GetComponent<NavMeshSurface>());
                    GameObject t = Instantiate(trigger, parent.transform);
                    t.transform.localPosition = obj.transform.position;
                    Walls(i, q, obj);
                    t.GetComponent<changeRoom>().setRoom(obj);
                } else if (maze[i, q] == 1)
                {
                    int rand = Random.Range(0, room.Length - 1);
                    GameObject obj = Instantiate(room[rand], parent.transform);
                    BoxCollider col = obj.GetComponentInChildren<BoxCollider>();
                    obj.transform.position = new Vector3(-(col.bounds.extents.x + (col.bounds.size.x * i)), 0, col.bounds.extents.z + (col.bounds.size.z * q));
                    map.Add(obj);
                    surface.Add(obj.GetComponent<NavMeshSurface>());
                    GameObject t = Instantiate(trigger, parent.transform);
                    t.transform.localPosition = obj.transform.position;
                    Walls(i, q, obj);
                    t.GetComponent<changeRoom>().setRoom(obj);
                }
            }
        }
    }

    /// <summary>
    /// Sets the maze to see if the player can get from the start point to another point (usually the end or special room).
    /// </summary>
    /// <param name="endSpace"></param>
    /// <returns></returns>
    bool MazeTester(Vector2 endSpace)
    {
        Stack<Vector2> pos = new Stack<Vector2>();
        Stack<Vector2> past = new Stack<Vector2>();
        pos.Push(start);
        past.Push(pos.Peek());
        bool doesWork = false;
        int stop = 0;
        while (pos.Count > 0 && !doesWork)
        {
            if (pos.Peek().x == endSpace.x && pos.Peek().y == endSpace.y)
            {
                doesWork = true;
            }
            else if (pos.Peek().x + 1 < mazeSize && maze[(int)pos.Peek().x + 1, (int)pos.Peek().y] == 1 && !past.Contains(new Vector2(pos.Peek().x + 1, pos.Peek().y)))
            {
                pos.Push(new Vector2(pos.Peek().x + 1, pos.Peek().y));
                past.Push(pos.Peek());
            }
            else if (pos.Peek().x - 1 >= 0 && maze[(int)pos.Peek().x - 1, (int)pos.Peek().y] == 1 && !past.Contains(new Vector2(pos.Peek().x - 1, pos.Peek().y)))
            {
                pos.Push(new Vector2(pos.Peek().x - 1, pos.Peek().y));
                past.Push(pos.Peek());
            }
            else if (pos.Peek().y + 1 < mazeSize && maze[(int)pos.Peek().x, (int)pos.Peek().y + 1] == 1 && !past.Contains(new Vector2(pos.Peek().x, pos.Peek().y + 1)))
            {
                pos.Push(new Vector2(pos.Peek().x, pos.Peek().y + 1));
                past.Push(pos.Peek());
            }
            else if (pos.Peek().y - 1 >= 0 && maze[(int)pos.Peek().x, (int)pos.Peek().y - 1] == 1 && !past.Contains(new Vector2(pos.Peek().x, pos.Peek().y - 1)))
            {
                pos.Push(new Vector2(pos.Peek().x, pos.Peek().y - 1));
                past.Push(pos.Peek());
            }
            else
            {
                pos.Pop();
                stop++;
                if (stop >= 500) break;
            }
        }
        return doesWork;
    }

    /// <summary>
    /// Sets the walls for a room.
    /// </summary>
    /// <param name="roomX"></param>
    /// <param name="roomY"></param>
    /// <param name="room"></param>
    void Walls(int roomX, int roomY, GameObject room)
    {
        GameObject eastWall;
        GameObject westWall;
        GameObject northWall;
        GameObject southWall;
        if (maze[roomX, roomY] == 1)
        {
            if (roomX + 1 < mazeSize && maze[roomX + 1, roomY] == 1)
            {
                westWall = Instantiate(openWall, room.transform);
            } else
            {
                westWall = Instantiate(closedWall, room.transform);
            }
            if (roomX - 1 >= 0 && maze[roomX - 1, roomY] == 1)
            {
                eastWall = Instantiate(openWall, room.transform);
            } else
            {
                eastWall = Instantiate(closedWall, room.transform);
            }
            if (roomY + 1 < mazeSize && maze[roomX, roomY + 1] == 1)
            {
                northWall = Instantiate(openWall, room.transform);
            } else
            {
                northWall = Instantiate(closedWall, room.transform);
            }
            if (roomY - 1 >= 0 && maze[roomX, roomY - 1] == 1)
            {
                southWall = Instantiate(openWall, room.transform);
            } else
            {
                southWall = Instantiate(closedWall, room.transform);
            }
            northWall.transform.localPosition = new Vector3(0f, 3.6f, 11f);
            northWall.transform.rotation = Quaternion.Euler(new Vector3(15f, 0, 0));
            eastWall.transform.localPosition = new Vector3(11f, 3.6f, 0f);
            eastWall.transform.rotation = Quaternion.Euler(new Vector3(15f, 90f, 0));
            westWall.transform.localPosition = new Vector3(-11f, 3.6f, 0f);
            westWall.transform.rotation = Quaternion.Euler(new Vector3(-15f, 90f, 0));
            southWall.transform.localPosition = new Vector3(0f, 3.6f, -9.1f);
            southWall.transform.rotation = Quaternion.Euler(new Vector3(15f, 0, 0));
            southWall.tag = "SouthWall";
            Renderer[] rend = southWall.GetComponentsInChildren<Renderer>();
            foreach (Renderer r in rend)
            {
                r.material.color = new Color(r.material.color.r, r.material.color.g, r.material.color.b, 0f);
            }
        }
          
    }

    public GameObject getStartRoom()
    {
        return startingRoom;
    }

    public List<GameObject> getMap()
    {
        return map;
    }
}
